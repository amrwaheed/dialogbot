// dialogflow-gqujkr@angularbot-bnpypa.iam.gserviceaccount.com
//5164fb547bfb14a33e89215c605cb57ae5655d81
/*
Dialogflow Integrations
Email address
dialogflow-gqujkr@angularbot-bnpypa.iam.gserviceaccount.com
Key IDs
5164fb547bfb14a33e89215c605cb57ae5655d81
App Engine default service account
Email address
angularbot-bnpypa@appspot.gserviceaccount.com
Key IDs
f91cef5a82d5dcae969213d0845470eb3b0d335d
*/

const dialogflow = require('dialogflow');
const uuid = require('uuid');
const path = require('path');

const express = require('express');

const app = express()

const port = 5000;

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(function (req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.post('/sendMessage', (request, responses) => {

  runSample(request.body.message).then(data => {
    responses.send({ 'Replay': data })
  })
});



  // A unique identifier for the given session

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */
async function runSample(msg, projectId = 'angularbot-bnpypa') {
  const sessionId = uuid.v4();


  // Create a new session
  // Create a new session
  const sessionClient = new dialogflow.SessionsClient({
    keyFilename: path.join(__dirname, 'AngularBot-9e7db7c485d2.json')
  });
  const sessionPath = sessionClient.sessionPath(projectId, sessionId);

  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        // The query to send to the dialogflow agent
        text: msg,
        // The language used by the client (en-US)
        languageCode: 'en-US',
      },
    },
  };

  // Send request and log result
  const responses = await sessionClient.detectIntent(request);
  console.log('Detected intent');
  const result = responses[0].queryResult;
  console.log(`  Query: ${result.queryText}`);
  console.log(`  Response: ${result.fulfillmentText}`);
  if (result.intent) {
    console.log(`  Intent: ${result.intent.displayName}`);
  } else {
    console.log(`  No intent matched.`);
  }
  return result.fulfillmentText
}

app.listen(port, () => {console.log('running on port '+port)})
