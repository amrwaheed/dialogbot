import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiAiClient } from 'api-ai-javascript/es6/ApiAiClient';

import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export class Message {
  constructor(public content:string, public sentBy:string){}
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  readonly token = environment.dialogflow.angularBot;
  readonly client = new ApiAiClient({accessToken:this.token})
  conversation = new BehaviorSubject<Message[]>([]);
  constructor(
  private http:HttpClient
  ) { }

  /**
   * Add message to source
   * @param msg
   */
  update(msg: Message){
    this.conversation.next([msg]);
  }
   postMessage(message:string){
     console.log(message)
    return this.http.post('http://localhost:5000/sendMessage',{message})
   }

  /**
   * Sends and receives messages vai Dialogflow
   * @param msg
   */
  converse(message: string){
    const userMessage = new Message(message, 'user');
    this.update(userMessage);

    return this.http.post('http://localhost:5000/sendMessage',{message}).subscribe(res => {
      const speech = res['Replay'];
      const botMessage = new Message(speech, 'bot');
      this.update(botMessage);
    })

  }

  talk(){
    this.client.textRequest('Who are you!').then(res => console.log(res))
  }

}
