import { Component, OnInit } from '@angular/core';
import { ChatService, Message } from '../chat.service';
import { Observable } from 'rxjs';
import { scan } from 'rxjs/operators';
@Component({
  selector: 'chat-dialog',
  templateUrl: './chatDialog.component.html',
  styleUrls: ['./chatDialog.component.css']
})
export class ChatDialogComponent implements OnInit {

  messages: Observable<Message[]>;
  formValue: string;

  constructor(private chat: ChatService) { }

  ngOnInit() {
    // this.chat.talk();
    this.messages = this.chat.conversation.asObservable()
      .pipe(
        scan((acc, val) => acc.concat(val))
      );
  }

  sendMessage() {
    this.chat.converse(this.formValue);
    this.formValue = '';

    // this.chat.postMessage(this.formValue).subscribe(res =>{
    //   console.log(res)
    // })

  }

}
