// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  dialogflow:{
    angularBot: '5164fb547bfb14a33e89215c605cb57ae5655d81',
    client:"158620401805-ekqv1ada3e2gp4leinh7ocsfm15lhd3p.apps.googleusercontent.com",
    clientsecret:"3aFFkCB_l74oh9ReupiJNoeb"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
